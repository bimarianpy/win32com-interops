#-------------------------------------------------------------------------------
# Name:        comnote.py
# Purpose:     display the use of win32com library for interfacing the
#              Microsoft OneNote 2010 application.
# Author:      n.chaterjee@bimarian.com
#
# Created:     05-05-2013
# Copyright:   (c) bimarian.com 2013
# Licence:     MIT
#-------------------------------------------------------------------------------
import re
import win32com.client as comClient
from xml.dom.minidom import parseString


class OneNote(object):
    """ class provides com methods for OneNote application """

    def __init__(self):
        """ create a late binding instance of MS OneNote application"""
        self.note_app = comClient.Dispatch("OneNote.Application")

    def getNoteBooks(self):
        """ displays the notebooks that are available with the OneNote """

        # from : http://msdn.microsoft.com/en-us/library/office/gg649853(v=office.14).aspx
        #
        # HRESULT GetHierarchy(
        # [in]BSTR bstrStartNodeID,
        # [in]HierarchyScope hsScope,
        # [out]BSTR * pbstrHierarchyXmlOut,
        # [in,defaultvalue(xs2010)]XMLSchema xsSchema);
        #
        # from : http://msdn.microsoft.com/en-us/library/office/ff966473(v=office.14).aspx
        #
        # HierarchyScope
        # When passed to the GetHierarchy method, specifies the lowest level
        # to get in the notebook node hierarchy.
        # note_app.GetHierarchy("", 2)
        #
        # Member	 Value	 Description
        # hsSelf      0      Gets just the start node specified and no descendants.
        # hsChildren  1      Gets the immediate child nodes of the start node, and no descendants in higher or lower subsection groups.
        # hsNotebooks 2      Gets all notebooks below the start node, or root.
        # hsSections  3      Gets all sections below the start node, including sections in section groups and subsection groups.
        # hsPages     4      Gets all pages below the start node, including all pages in section groups and subsection groups.

        all_notes = self.note_app.GetHierarchy("", 2)

        # parse the xml
        dom = parseString(all_notes)
        notes_objects = dom.documentElement.childNodes

        # iterate to form the dictionary of notes data
        note_data = []

        for note in notes_objects:
            if not note.childNodes:  # no child nodes are there for the note
                note_name = note.getAttribute('name')
                note_dt = note.getAttribute('lastModifiedTime')
                note_uri = note.getAttribute('path')
                # find if the note belongs to skydrive
                if re.search("^http", note_uri):
                    note_type = "Web"
                else:
                    note_type = "FileSystem"

                note_data.append({'name': note_name,
                'date': note_dt,
                'location': note_uri,
                'availability': note_type,})

        return note_data

def main():
    # create an instance of the com wrapper.
    mynote = OneNote()
    mynote_data = mynote.getNoteBooks()
    # print the data in formatted text.
    if mynote_data:
        # print the headers.
        print "Notes Listing:", "\n"
        print "%-15s%-25s%-15s%s" %("Location","Date Last Modified",
                                "Filename", "Filepath")
        print "="*100
        for note in mynote_data:
            print "%(availability)-15s%(date)-25s%(name)-15s%(location)s" % note

if __name__ == '__main__':
    main()
